FROM mysql:5.7
MAINTAINER RADIM PESA <pesa@ics.muni.cz>


ADD https://raw.githubusercontent.com/cBioPortal/cbioportal/v2.0.0/db-scripts/src/main/resources/cgds.sql /tmp/
ADD https://github.com/cBioPortal/datahub/blob/master/seedDB/seed-cbioportal_hg19_v2.7.3.sql.gz?raw=true /tmp/seed-cbioportal_hg19_v2.7.3.sql.gz
ADD files /tmp/
RUN chmod +x /tmp/cbiodb.sh
