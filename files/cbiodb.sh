#!/bin/bash
echo on

mysql -e "GRANT ALL ON *.* to cbio@'%' IDENTIFIED BY 'P@ssword1';"
mysql -e "create database cbioportal;"
cat /tmp/cgds.sql | mysql cbioportal
cat /tmp/seed-cbioportal_hg19_v2.7.3.sql.gz | gunzip | mysql cbioportal
